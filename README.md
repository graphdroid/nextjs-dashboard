# HomeStay - NextJS and Tailwind Template

## About

HomeStay is a responsive landing page template which can be used for booking home stays for different occasions developed using nextjs, tailwind for frontend and postrgres for database. It is mostly suitable for small businesses because it has only the necessary features like checking the availability of home stays, scheduling the date and connecting the client with the owner.

## Features
**Note: Some of the features can be or will be added in the future.**
1) Details and availability.
2) The map is intrigated to reach the destination.
3) Can directly contact with the owner.
4) Feedback and ratings.
5) Scheduling the date.


## What's included
**Note: Some of the features can be or will be added in the future.**
1. Landing page
2. Admin
    * Login
    * Dashboard
3. Typography
4. UI Components
    * Cards
    * Buttons
    * Navbar
    * Tables

## Languages and Frameworks
1. NextJs
2. Tailwind
3. React
4. TypeScript
5. Postgres

## Installation
**Note: The latest version of NodeJS and npm has to be installed in the system.** 
1. Download the source code from Gitlab.
2. Open the source code in IDE or any text editor.
3. Open the terminal in your IDE or any text editor.
4. Run the `npm install` command in your terminal.

## Credits
The images, icons and illustrations are used from unsplash, material design icons and freepik.

## License
MIT License

## Contact
For any grivences and feedbacks or any legal procedings arising from my project.
<br />
<br />
graphdroid.190@proton.me

## Author
Anand Limbu | Graphdroid

**Copyright &copy; 2019**
